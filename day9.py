# #TODO: add urllib to readme
# #constructor with
#
import sqlite3

class DemoConstructor:
    def __init__(self, database):
        self.database = database
        self.connect = None

    def __enter__(self):
        self.connect = sqlite3.connect('kuna.db')
        return self

    def happy(self):
        print("i am happy")

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connect.close()
        print("successfully exited")
#
#
#
# with DemoConstructor() as kuna:
#     print("i am in middle")
# text file reading
# k = open('demo_text.txt')
# for each_line in k.readlines():
#     print(each_line)
#to read a particular line
# h = k.readlines()
# print(h[1])
# # k.close()


# with open('demo_text.txt', 'r') as k:
#     for each_line in k.readlines():
#         print(each_line)
#
# import csv
#
# k = open('democsv.csv', 'r')
# print(dir(csv))
# data = csv.DictReader(k)
# for each in data:
#     print(each['marks'])






