#sorting

# k = [1,2,6,2,5,7,4]
# print(dir(k))
# k.sort()
# print(k)

# print(dir(k))
# k.sort(reverse=True)
# print(k)

#this is for different clone

# k = [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
#
# # def name(a):
# #     return a[1]
#
# k.sort(key=lambda a : a[1])
# print(k)

# #loops

# for k in range(0,50):
#     if k > 10:
#         break
#     else:
#         print(k)
# for each in range(50, 100):
#     for k in range(0,50):
#         if k > 10:
#             break
#         else:
#             print(k)
#     print(each)

# # g = sorted(range(10,1,-1), reverse=1)
# # print(g)
# k = [1,2,3,4]
# for each in k:
#     if each > 2:
#         break
#     else:
#         print(each)
# else:
#     print("else executed sucessfully")

#pass and continue

# for each in range(0,20):
#     if each > 5 and each <15:
#         continue
#     print(each, "hi")

# for each in [1,2,3,4,5]:
#     if each < 3:
#         print(each)
#         continue
#     print(each, "my name is kuna")
#     print(each, "my age is 25")
#     print(each, "i am male")

# #list comprehesion
#
# k = range(0,100)
# print(list(k))
#
# k = [x for x in range(0,100) if not (x>5 and x<15) ]
# print(k)
#
# k = [x  if x>5 else "hi" for x in range(0,100)]
# print(k)










