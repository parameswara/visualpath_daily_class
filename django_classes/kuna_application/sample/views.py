from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django import views
from .models import kuna_orm


from rest_framework.serializers import ModelSerializer
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

class ListSerializer_kuna(ModelSerializer):
    class Meta:
        model = kuna_orm
        fields = ["age", "name", "rel_project"]


# Create your views here.


def kuna(request):
    return HttpResponse("Hello World")

def kuna_integer(request, pk):
    return HttpResponse(f"Hi you have given value {pk}")

def kuna_integer_and_char(request, pk, ab):
    return HttpResponse(f"Hi you have given value {pk} and {ab}")

def kuna_re(request):
    return HttpResponse("regular expression")


class kuna_with_post_get(views.View):
    def get(self, request):
        k = []
        j = kuna_orm.objects.all()
        for each in j:
            k.append([each.name, each.age, each.gender, each.email])
        return JsonResponse({"data": k})
    def post(self, request):
        return JsonResponse("post method invoked")

class drf_kuna(GenericAPIView):
    serializer_class = ListSerializer_kuna
    queryset = kuna_orm.objects.all()
    def get(self, request):
        import pdb
        pdb.set_trace()
        j = kuna_orm.objects.all()
        serial_data = ListSerializer_kuna(j, many=True)
        return Response(serial_data.data)

    def post(self, request):
        serial_data = ListSerializer_kuna(data=request.data)
        if serial_data.is_valid():
            serial_data.save()
        return Response(serial_data.data)
