from django.urls import path, re_path
from .views import *




urlpatterns = [
    path("", kuna),
    path("<int:pk>/", kuna_integer),
    path("<int:pk>/<str:ab>/", kuna_integer_and_char),
    re_path(r"\w+\d\w+/", kuna_re),
    path("kuna_fun/", kuna_with_post_get.as_view()),
    path("rest_kuna/", drf_kuna.as_view()),
]