from django.db import models

gender = (("male","Male"),("female", "Female"))
# Create your models here.
# delete auto generated migration file if edit is required
#
class project(models.Model):
    project_name = models.CharField(max_length=20, default=None, blank=True)

    def __str__(self):
        return f"project is {self.project_name}"

class kuna_orm(models.Model):
    name = models.CharField(max_length= 20)
    age = models.IntegerField(default=0, blank=True)
    email = models.EmailField(blank=True)
    gender = models.CharField(max_length=6, choices=gender, default=gender[0][0], blank=True)
    rel_project = models.ForeignKey('project', on_delete=models.CASCADE, blank=True, default=0, null=True)

