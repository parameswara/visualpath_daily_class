from django.contrib import admin
from .models import kuna_orm, project
# Register your models here.


admin.site.register(kuna_orm)
admin.site.register(project)