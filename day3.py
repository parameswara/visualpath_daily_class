# list initilize
#mutable
k = [1,2,3]
#print(k)

#print("*" * 30)

#type

#print(type(k))

#dir

#print(dir(k))

#'append'
#k.append('hi')
#print(k)

#'clear'
#k.clear()
#print(k)

#'copy'

b = k.copy()
print(k)
print(b)

d = b

print(d)

d[0] = 8

print(d)
print(b)

k = [1,2,3,4,5,6,3]

#'count'
print(k.count(3))

#'extend'
g = ["h", "k", "l"]
#k.append(g)
#print(k)
k.extend(g)
print(k)

k = ["hi", "bye", "tata"]
#'index'

print(k.index('hi'))


#'insert'
print(k)
k.insert(1, "element")
print(k)


#'pop',

k.pop()
print(k)

k.pop(0)
print(k)

#'remove'
k.remove("element")
print(k)

k.extend(['apple', 'banana'])
#'reverse'

print(k)
k.reverse()
print(k)

k.sort()
print(k)


#split a string

k = "apple;banana;mango"

print(k)

print(k.split(';'))

g = k.split(';')
print(".".join(g))

h = "parameh@gmail.com"

#welcome to <yahoo> mr <name>



