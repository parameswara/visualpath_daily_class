# dictionary

# key, value
# key is unique
# key should be immutable

#create a dictionary (dict)

#k = {"first_key": "hi"}
#print(k)
#print(type(k))

#create a dict using list
#a = ["frist_key", "second_key"]
#b = ["hi", "bye"]
#k = dict(zip(a, b))
#print(k)

#create a dict using tuples

#g = (("frist_key", "hi"), ("second_key", "bye"))
#k = dict(g)
#print(k)

# attributes

k = {"first_key": "hi"}
#print(dir(k))

#'clear'
#k.clear()
#print(k)

k = {"first_key": "hi"}
#'copy'
#b = k.copy()
#print(b)

#'fromkeys'
#z = k.fromkeys(["first_key"])
#print(z)
#print(k)

#k["second_key"] = None
#print(k)

k = {"first_key": "hi"}
#'get'
#print(k.get("first_key", "no key"))
#print(k["first_key"])

#TODO
#'items'

k = {"first_key": "hi"}
#'keys'
#print(k.keys())

#'values'
#print(k.values())

#k = {"first_key": "hi", "second_key":"bye"}

#'pop'
#k.pop("first_key")
#print(k)

#TODO
#'popitem', 'setdefault', 'update',

#k = {"first_key": "hi", "second_key":"bye"}
#'setdefault'
#k.setdefault("rao")
#print(k)

#'update'
#k = {"first_key": "hi", "second_key":"bye"}
#z = {"third_key": "hi", "fourth_key":"bye"}

#k.update(z)
#print(k)

# for loop

#k = ["first", "sec", "third"]

#for each in k:
#    print(each)

#k = [["first", "second"],["third", "fourth"]]
#print(k[0][0])

#for val_1, val_2 in k:
#    print(val_1, val_2)

#items
#k = {"first_key": "hi", "second_key":"bye"}
#for key, value in k.items():
#    print(key, value)


















