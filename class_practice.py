import csv

csv_temp = open('practice.csv', "r")
csv_data = csv.DictReader(csv_temp)
for each in csv_data:
    with open(each['file'], 'r') as text_temp:
        for each_line in text_temp:
            print(f"this is {each['name']} corresponding to {each['file']} with content {each_line}")