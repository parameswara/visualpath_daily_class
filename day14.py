#!/bin/python3
#
# #
# class happy():
#     age = 25
#     def __init__(self, name):
#         self.name = name
#
#     def some_fun(self):
#         print(self.name)
#         print(self.age)
#
#     @classmethod
#     def other_fun(cls):
#         cls.name = "kuna"
#         cls.some_fun(cls)
#
#
# k = happy("rao")
# k.other_fun()
# k.age = 45
# k.some_fun()


# #property
#
# class happy():
#     def __init__(self, name):
#         self.name = name
#         self.age = 10
#
#     def print_fun(self):
#         print(self.name)
#
#     def age_cal(self):
#         if self.name == "pavan":
#             self.age += 10
#         else:
#             self.age +=30
#
#     @property
#     def load_fun(self):
#         return self.name
#
#     @load_fun.setter
#     def  load_fun(self, value):
#         if len(value) < 4:
#             raise ValueError("Name should be more than 4 char")
#         self.name = value
#
# k = happy('pavan')
# k.print_fun()
# k.age_cal()
# k.load_fun = "rao123"
# k.print_fun()
# print(k.age)
import os
import sys
print(os.name)
print(sys.platform)



