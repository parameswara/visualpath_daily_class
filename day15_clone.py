from day15 import hi
import unittest

class test_hi(unittest.TestCase):
    def test_hi_fuc(self):
        name, age = hi("kuna", 35)
        self.assertEqual(name, "kuna1")
        self.assertEqual(age, "351")

