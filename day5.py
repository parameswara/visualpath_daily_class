# #conversions
#
# #string to int
# k = "1"
# print(type(int(k)))

# #int to string
# k = 1
# z = str(k)
# print(type(z))
#
# # any
# # Any element is true output is true
# # Except zero, None and false rest all are true
# k = [0, False, None]
# print (any(k))
# k = [True, False, None]
# print (any(k))

# # all
# k = [True, True]
# print(all(k))
# k = [True, 0]
# print(all(k))

# #bool
# k = "1"
# print (bool(k))
# # string zero "0" will be considered as True
# print (bool(0))

# #eval
# # it will understand the string and exec it as a statement
# # a = 1
# # b = 2
# # k = "ab"
# # z = eval("a + b")
# # print(z)
#
# # #For int the address is fixed (reference)
# # k = 1
# # print(id(k))
# #
# # z = 1
# # print(id(z))
# #
# # k = "1"
# # print(id(k))
#
# #TODO: sum, max, min
# #TODO: try max and min
# k = [1,2,3]
# print(sum(k))


# #function
#
# # def <fun name>(arguments):
# #     statements
# #     return <output>
#
# # def nam():
# #     a = 1
# #     b = 2
# #     print(a + b)
# # nam()
#
# # def nam(a,b):
# #     print(a + b)
# #
# # nam(1,2)
# #
# # nam(3,4)
#
def nam(a,b=2,c=7):
    """

    :param a:
    :param b:
    :param c:
    :return:
    """
    print(a + b + c)
# #
# # nam(5)
# #
# # nam(5,5)
# #
# # nam(5,c=5)
#
# # argument or method overloading
# # giving n number of arguments
#
# def name(a, b, *args):
#     print(args[2])
#
#
# name("1", 2, "hi", "bye", "first")


