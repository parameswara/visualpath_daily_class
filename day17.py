from flask import Flask, request
app = Flask(__name__)

@app.route('/')
def hello_world():
    k = request.args["name"]
    age = request.args["age"]
    return '<head>' \
           '<title> POST request </title>' \
           '<body>' \
            '<p> welcome {} with age {}</p>'\
           '<form action="/index" method="post">' \
            'Name <input type="text" name="fname"></input>' \
           '<button title="submit" >SUBMIT</button>' \
           '</form>' \
           '</body>' \
           '</head>'.format(k, age)

@app.route('/index', methods=["post"])
def happy():
    k = request.form["fname"]
    return f"{k}logged sucessfully"


if __name__=="__main__":
    app.run("0.0.0.0")


