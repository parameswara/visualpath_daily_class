# func with kwargs

#def name(*args,**kwargs):
#    print(kwargs)
#    print(args)
#

#name(4,5,6, a=1,b=2,c=3)
#func stops executing when ever it hits return statement
#After returning it forgets the data inside it.

#k = "kuna"

#def name(a,b,c):
#    h = a+b+c
#    k = "rao"
#    print("k in function",k)
#    return h

#print("k in global", k)
#result = name("hi", "bye", "first")
#print(result)


k = [1,[2,[3,[4,5]]]]
z = []

def name(a):
    if str(a).isalnum():
        z.append(a)
    else:
        z.append(a[0])
        name(a[1])

result = name(k)
print(z)



    





