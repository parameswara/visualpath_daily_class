# # arguments
# import sys
#
# def hi(args):
#     for each_arg in args:
#         print(str(each_arg) + "1")
#
#
# if __name__ == "__main__":
#     print("i am in main")
#     print(type(sys.argv))
#     hi(sys.argv)

# #arg parse
#
# import argparse
#
# k = argparse.ArgumentParser()
# k.add_argument("--name", "-n", help="please give your name")
# k.add_argument("--age", "-a", type=int, help="please give your age")
#
# args = k.parse_args()
#
# print(args.name)
# print(args.age)


# unit testing

def hi(name, age):
    name_morph = str(name) + "1"
    age_morph = str(age) + "1"
    return name_morph,age_morph

name, age = hi("kuna", 35)
print(name, age)