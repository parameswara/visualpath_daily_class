# visualpath_daily_class

Day to day class in visualpath

30 days schedule (25 days excluding Sundays) <br />
 <br />
Day 1 <br />
10:00 to 10:10	Windows fundamentals,  <br />
10:10 to 10:20	python install, path, import <br />
10:20 to 10:30	pip, built in modules, custom modules <br />
10:30 to 10:40 	idle, IDE <br />
10:40 to 10:50	git repo,  <br />
10:50 to 11:00	git clone, add <br />
11:00 to 11:10	git commit, push <br />
11:10 to 11:20	LDAP, ACTIVE DIRECTORY, venv <br />
11:20 to 11:30	Python operators, print statement, type, dir() <br />
11:30 to 12:30	practice <br />
Day2 <br />
09.30 to 09.40	Variables, naming convention <br />
09.40 to 09.50	Data types – string, int, float <br />
10:00 to 10:10	raw input, <br />
10:10 to 10:20	String operators and expressions, Slicing <br />
10:20 to 10:30	isalnum, isalpha, istitle <br />
10:30 to 10:40 	replace <br />
10:40 to 10:50	rstrip <br />
10:50 to 11:00	 <br />
11:00 to 11:10	startswith, endswith <br />
11:10 to 11:20	zfill <br />
11:20 to 11:30	Format, def of remaining <br />
11:30 to 12:30	practice <br />
Day3 <br />
09.30 to 09.40	Lists <br />
09.40 to 09.50	Creating and Using Lists, string split <br />
10:00 to 10:10	sort(), reverse(), count(),swapping elements <br />
10:10 to 10:20	pop(), remove(), insert() and index(), set(list) <br />
10:20 to 10:30	Indexing, Slicing and Concatenating <br />
10:30 to 10:40 	in and len() <br />
10:40 to 10:50	copy <br />
10:50 to 11:00	deep copy <br />
11:00 to 11:10	examples on list <br />
11:10 to 11:20	examples on list <br />
11:20 to 11:30	examples on list <br />
11:30 to 12:30	practice <br />
 <br />
Day4 <br />
09.30 to 09.40	Dictionaries, creating methods, for loop, if loop <br />
09.40 to 09.50	Dictionary values and keys <br />
10:00 to 10:10	'keys', 'pop' <br />
10:10 to 10:20	'popitem' , 'setdefault' <br />
10:20 to 10:30	'update', 'values',  <br />
10:30 to 10:40 	'clear', 'copy', <br />
10:40 to 10:50	'fromkeys', 'get', 'items' <br />
10:50 to 11:00	Built in functions – all, any, bool,  <br />
11:00 to 11:10	Built in functions – eval, exec <br />
11:10 to 11:20	Built in functions – id, sum,  <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	Practice <br />
Day5 <br />
09.30 to 09.40	Revision <br />
09.40 to 09.50	Revision <br />
10:00 to 10:10	Revision <br />
10:10 to 10:20	Revision	 <br />
10:20 to 10:30	Revision	 <br />
10:30 to 12:30	Revision <br />
 <br />
 <br />
Day6 <br />
09.30 to 09.40	What are functions/methods <br />
09.40 to 09.50	arguments  <br />
10:00 to 10:10	keyword arguments <br />
10:10 to 10:20	local vs global variables <br />
10:20 to 10:30	yeild (generators/ iterators), next() <br />
10:30 to 10:40 	lambda <br />
10:40 to 10:50	filter <br />
10:50 to 11:00	map <br />
11:00 to 11:10	reduce <br />
11:10 to 11:20	sort using key (key will refer to function) <br />
11:20 to 11:30	
 <br />
 11:30 to 12:30	practice
 <br />
 Day7  <br />

pycharm installation  <br />


09.30 to 09.40	Loops and iterations  <br />


09.40 to 09.50	For, enumerate  <br />


10:00 to 10:10	break, continue, For else
 <br />

10:10 to 10:20	while	 <br />

10:20 to 10:30	if 	 <br />

10:30 to 10:40	if – else - nested 	  <br />
10:40 to 10:50	pass	 <br />
10:50 to 11:00	logical operators – conditions - and <br />
11:00 to 11:10	logical operators – conditions - or <br />
11:10 to 11:20	list comprehension <br />
11:20 to 11:30	examples on loops <br />
11:30 to 12:30	Practice <br />
 <br />
Day8 <br />
09.30 to 09.40	File operations and with <br />
09.40 to 09.50	database <br />
10:00 to 10:10	json/xml <br />
10:10 to 10:20	csv <br />
10:20 to 10:30	write csv <br />
10:30 to 10:40 	txt file <br />
10:40 to 10:50	read/write operations <br />
10:50 to 11:00	regular expression - match <br />
11:00 to 11:10	regular expression – find(all) <br />
11:10 to 11:20	regular expression – search <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	practice <br />
Day9 <br />
09.30 to 09.40	decorator <br />
09.40 to 09.50	Exceptions <br />
10:00 to 10:10	try block <br />
10:10 to 10:20	catch and finally <br />
10:20 to 10:30	multiple exceptions <br />
10:30 to 10:40 	 raising exceptions <br />
10:40 to 10:50	Queue operations <br />
10:50 to 11:00	collections <br />
11:00 to 11:10	examples <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	practice <br />
Day10 <br />
09.30 to 09.40	automation <br />
09.40 to 09.50	automation <br />
10:00 to 10:10	automation <br />
10:10 to 10:20	automation	 <br />
10:20 to 10:30	automation	 <br />
10:30 to 12:30	automation	  <br />
 <br />
 <br />
Day 11 <br />
10:00 to 10:10	 OOP Terminology <br />
10:10 to 10:20	INIT in class <br />
10:20 to 10:30	Methods <br />
10:30 to 10:40 	Built in attribute <br />
10:40 to 10:50	Encapsulation  <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	examples <br />
11:10 to 11:20	Data hiding <br />
11:20 to 11:30	Properties <br />
11:30 to 12:30	Practice <br />
Day12 <br />
09.30 to 09.40	Inheritance <br />
09.40 to 09.50	examples <br />
10:00 to 10:10	examples <br />
10:10 to 10:20	examples <br />
10:20 to 10:30	Ploymorphism <br />
10:30 to 10:40 	examples  <br />
10:40 to 10:50	examples <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	examples <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	examples <br />
Day13 <br />
09.30 to 09.40	staticmethod <br />
09.40 to 09.50	example <br />
10:00 to 10:10	example <br />
10:10 to 10:20	example <br />
10:20 to 10:30	example <br />
10:30 to 10:40 	 classmethod <br />
10:40 to 10:50	example <br />
10:50 to 11:00	example <br />
11:00 to 11:10	unittest <br />
11:10 to 11:20	example <br />
11:20 to 11:30	example <br />
11:30 to 12:30	practice <br />
 <br />
Day14 <br />
09.30 to 09.40	Packges <br />
09.40 to 09.50	data hiding <br />
10:00 to 10:10	examples <br />
10:10 to 10:20	examples <br />
10:20 to 10:30	examples <br />
10:30 to 10:40 	Operator overloading  <br />
10:40 to 10:50	examples <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	Multiple inheritance <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	Practice <br />
Day15 <br />
09.30 to 09.40	Revision <br />
09.40 to 09.50	Revision <br />
10:00 to 10:10	Revision <br />
10:10 to 10:20	Revision <br />
10:20 to 10:30	Revision <br />
10:30 to 12:30	Revision <br />
 <br />
 <br />
 <br />
 <br />
Day16 <br />
09.30 to 09.40	Meta classes <br />
09.40 to 09.50	examples <br />
10:00 to 10:10	Abstract classes <br />
10:10 to 10:20	examples <br />
10:20 to 10:30	examples <br />
10:30 to 10:40 	 Multi processing <br />
10:40 to 10:50	examples <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	examples <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	Practice <br />
Day17 <br />
09.30 to 09.40	Mathematical modules <br />
09.40 to 09.50	examples <br />
10:00 to 10:10	examples <br />
10:10 to 10:20	examples <br />
10:20 to 10:30	examples <br />
10:30 to 10:40	Sql queries  <br />
10:40 to 10:50	examples <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	examples <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	examples <br />
 <br />
Day18 <br />
09.30 to 09.40	Data persistence <br />
09.40 to 09.50	examples <br />
10:00 to 10:10	examples <br />
10:10 to 10:20	examples <br />
10:20 to 10:30	examples <br />
10:30 to 10:40 	logger <br />
10:40 to 10:50	examples <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	cron jobs <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	examples <br />
Day19 <br />
09.30 to 09.40	urllib <br />
09.40 to 09.50	json <br />
10:00 to 10:10	examples <br />
10:10 to 10:20	examples <br />
10:20 to 10:30	examples <br />
10:30 to 10:40 	 unix commands in python <br />
10:40 to 10:50	examples <br />
10:50 to 11:00	examples <br />
11:00 to 11:10	window commands in python <br />
11:10 to 11:20	examples <br />
11:20 to 11:30	examples <br />
11:30 to 12:30	examples <br />
Day20 <br />
09.30 to 09.40	Revision <br />
09.40 to 09.50	Revision <br />
10:00 to 10:10	Revision <br />
10:10 to 10:20	Revision <br />
10:20 to 10:30	Revision <br />
10:30 to 12:30	Revision <br />
 <br />
