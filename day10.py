# # database
#
# import sqlite3
#
# from day9 import DemoConstructor
# #
# with DemoConstructor("kuna.db") as database:
#     # print(dir(database))
#     database.connect.execute("CREATE TABLE IF NOT EXISTS demo (name VARCHAR(20), age int)")
#
#     database.connect.execute("INSERT INTO demo (name, age) VALUES ('pavan', 30)")
#     database.connect.commit()
#
#     result = database.connect.execute("SELECT * from demo")
#     k = result.fetchall()
#     print(k)

#  # json file
#
# import json
#
# k = open("paramesh.json", 'r')
# data = json.load(k)
# print(data)

# regular expression
#findall, match, search

import re


#r won't consider escape char
#^ is start of string
#$ end of string
#/d decimal and int

# \d,  \s | \S,  \w | \W | \w   trailing +

# \s stands for space
# \S stangs for string
# \w stands for alpha and numeric
# \W stands for special char and spaces
# \d stands for int and decimal
# + will render the data till space comes
k = "Hi this@ is kuna with age 33 , 1car and 1bike @hyd"

# result = re.findall(r"\w", k)
# print(result)
#
# result = re.findall(r"\w+", k)
# print(result)
#
# result = re.findall(r"\W", k)
# print(result)
#
# result = re.findall(r"\W+", k)
# print(result)
#
# k = "Hi this@ is kuna with age 33 , 1car and 1bike @hyd"
#
# result = re.findall(r"\d", k)
# print(result)
#
# result = re.findall(r"\d+", k)
# print(result)
#
# # [] it gives all custom parsing
# k = "Hi this@ is kuna with age 33 , 1car and 1bike @hyd"
# result = re.findall(r"[\w\s@]", k)
# print(result)
#
# #[^] its contridiction of []
# result = re.findall(r"[^\w\s@]", k)
# print(result)
#
# # .
# result = re.findall(r".", k)
# print(result)
#
# result = re.findall(r"...", k)
# print(result)
#
# result = re.findall(r".*", k)
# print(result)

# ^ $
# k = "Hi this@ is Kuna with age 33 , 1car and 1bike @hyd"
#
# result = re.findall(r"^[A-Z].*", k)
# print(result)
#
# k = "Hi this@ is Kuna with age 33"
# # k = "K100"
# result = re.findall(r"^[A-Z].*\d+$", k)
# print(result)

# # ?
# k = "zrererem"
# result = re.findall(r"z??m", k)
# print(result)


# # {}
# k = "1988"
#
# result = re.findall(r"\d{2}", k)
# print(result)

# # ()
# k = "Hi this@ is kuna with age 33 , 1car and 1bike @hyd"
# result = re.findall(r"(\w+)\s(\d+)", k)
# print(result)

#
# k = "Hi this@ is kuna with age 33 , 1car and 1bike @hyd"
# result = re.sub(r"\d{1}[a-z]+", "modified", k)
# print(result)


# k = "Hi this@ is kuna with age 33 , 1car and 1bike @hyd"
# #
# # pattern = re.compile(r"\d")
# #
# # result = re.(pattern, k)
# # for each in re:
# #     print(each)

# #exceptions
# k1 = "hi"
# try:
#     print(k1 + 3)
# except (NameError, ValueError):
#     print("check your variables please")
# except TypeError:
#     print("runtime error occured")
# finally:
#     print("i am final statement")


# try:
#     print(k1 + 3)
# except (NameError, ValueError) as e:
#     print("check your variables please")
#     print(str(e))
# except TypeError:
#     print("runtime error occured")
# else:
#     print("i am else")


# k = 2
#
# if k == 1:
#     print("hey i am okay")
# else:
#     raise ValueError("i will not accept other than one")








