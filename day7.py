#yield statement
#iterators and generators

# # range
# #range gives a list with numbers in a incremented fashion
# k = range(0,100)
# print(list(k))

# def fun_name():
#     for each in range(0,10):
#         yield each
#
# k = fun_name()
# print(dir(k))
# # print(k.__next__())
# # print(k.__next__())
# k1 = next(k)
# print(k1)
# k2 = next(k)
# print(k2)
# # for each in k:
# #     if each > 5:
# #         break
# #     else:
# #         print(each)

# def addition(a):
# #     return a + 2
# #
# # k = addition(1)
# # print(k)
# # k = addition(-5)
# # print(k)
# # # filter throws the input to output if and only if funtion returns true
# # result = filter(addition, [1, -5, 3, -2, -10])
# # print(list(result))

# #lambda input: return statement
# # result = filter(lambda a : a+2, [1, -5, 3, -2, -10])
# # print(list(result))
#
# # map takes the input from list and results it in output
# result = map(lambda a : a[0]+a[1]+2, [[1,0], [2,1], [3,2]])
# print(list(result))

# import functools
#
# # reduce
# # [1,2,3,4,5,6]
#
# result = functools.reduce(lambda a,b : a+b, [1,2,3])
# print(result)


#loops
# if __iter__ exists for a dir(variable) then we can run "for loop" on top of that
# k = [1,2,3,4,5]
# # print(dir(k))
#
# for each_item in range(50,80):
#     print(each_item)
#
# k = {1:"hi", 2:"bye", 3:"sad"}
# print(dir(k))
#
# for each_element in k:
#     print(each_element)
# print(dir(k.items()))
#
# for element_one, element_two in k.items():
#     print(element_one, element_two)
#
# for k in range(0,10):
#     for j in range(10, 20):
#         print(k,j)


#underscore (pep8)
# for _ in range(0,51):
#     print("hi")

#enum
k = ["hi", "bye", "first"]
for index, each  in  enumerate(k):
    print(index, each)

# # #if loop
# k = "hi"
# if k=="hi" and len(k) > 2:
#     print("satisfied")











