# #decorator
# # #wrapper to your return statement
# # def sample_decorator(fun):
# #     def one_more(*args):
# #         result = fun(*args)
# #         result += "welcome to my world"
# #         return result
# #     return one_more
# #
# # @sample_decorator
# # def  demo_fun(name):
# #     return name
# #
# # result = demo_fun("kuna")
# # print(result)

# def sample_decorator(fun):
#     def one_more(*args):
#         result = fun(*args)
#         result = f"<head><title>Welcome world</title><body>{result}</body></head>"
#         return result
#
#     return one_more
#
# @sample_decorator
# def  demo_fun(name):
#     return name
#
#
# def website():
#     # html_data = "<head><title>Welcome world</title><body>This is a new world</body></head>"
#     html_data = demo_fun("this is a new world")
#     with open("index.html", 'w') as html:
#         html.write(html_data)
#     print("done")
#
# website()
#
# def website2():
#     # html_data = "<head><title>Welcome world</title><body>This is a new world</body></head>"
#     html_data = demo_fun("this is a old world")
#     with open("index_old.html", 'w') as html:
#         html.write(html_data)
#     print("done")
#
# website2()

#TODO:__new__
# class class_name():
#     def __init__(self):
#         pass
#     def kuna(self, name):
#         print(name)
#
#
# h = class_name();
# h.kuna("paramesh")


# class class_name():
#     def __init__(self, name):
#         self.name = name
#
#     def kuna(self):
#         print(self.name)
#
#
# h = class_name("paramesh");
# h.kuna()


class class_name():
    def __init__(self, name):
        self.name = name
        self.kuna()

    def kuna(self):
        print(self.name)


h = class_name("paramesh");


