# # static method vs class method
#
# def third_fun():
#     print("i am rao")
#
# class sampleclass():
#     def __init__(self):
#         self.name = "kuna"
#         self.age = 25
#         self.gender = "male"
#
#     def first_fun(self):
#         print (self.name)
#         self.sec_fun("paramesh")
#         self.copy_fun()
#
#     def another_fun(self):
#         print(self.name)
#         print(self.age)
#
#     @staticmethod
#     def sec_fun(name):
#         print("i am rao")
#         print(name)
#
#     def one_more(self):
#         print(self.gender)
#
#
#     @classmethod
#     def copy_fun(cls):
#         cls.name = "ravi"
#         cls.age = 30
#         cls.another_fun(cls)
#
#     @classmethod
#     def copy_one(cls):
#         cls.gender = "female"
#         cls.one_more(cls)
#
# # third_fun()
# k = sampleclass()
# # k.first_fun()
# # k.sec_fun("iam another kuna")
# k.copy_fun()
# k.copy_one()


#__call__
#__new__
class sampleclass(hi, bye):
    def __init__(self):
        self.name = "kuna"
        self.count = 0

    def __call__(self):
        print(self.name)

    def incr_count(self):
        self.count += 1
        return self.count

    def print_world(self, name):
        print('hi' + name)

    def __iter__(self):
        yield self.incr_count

    def __getattribute__(self, item):
        return




k = sampleclass()
# for each in k:
#     print(each())
#     print(each())
#     print(each())
# print(k.happy)

print(k.print_world("kuna"))

