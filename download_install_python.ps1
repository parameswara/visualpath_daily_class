[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
$url = "https://www.python.org/ftp/python/3.7.0/python-3.7.0-amd64.exe"
$output = "python_install.exe"
$start_time = Get-Date

Invoke-WebRequest -Uri $url -OutFile $output
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

.\python_install.exe /quiet InstallAllUsers=0 PrependPath=1 Include_test=0