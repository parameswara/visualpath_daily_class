from flask import Flask, request, render_template
app = Flask(__name__)

@app.route('/')
def hello_world():
    # k = request.args["name"]
    # age = request.args["age"]
    return render_template('simple.html')

@app.route('/index', methods=["post"])
def happy():
    k = request.form["fname"]
    return f"{k}logged sucessfully"


if __name__=="__main__":
    app.run("0.0.0.0")


