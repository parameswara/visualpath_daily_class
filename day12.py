# #encapsulation
#
# #nonlocal, global
# class hi():
#     happy = "i have a value"
#     def __init__(self):
#         self.name = "kuna"
#         self.__a = "value"
#     def fun_name(self):
#         g = "i am kuna"
#         self.name = "other kuna"
# k = hi()
# print(k.name)
#
# try:
#     print(k.__a)
# except AttributeError:
#     print("i am cannot get accessed")
#
# k.fun_name()
# print(k.name)

#inheritance
# class hi(object):
#     def __init__(self):
#         self.name = "kuna"
#
#     def happy(self):
#         print(self.name)
#
# class child(hi):
#     def __init__(self):
#         self.other_name = "common"
#         self.name = "rao"
#
#     def one_more(self):
#         print(self.other_name)
#
# k = child()
# print(k.happy())

# # using super
# class hi(object):
#     def __init__(self):
#         self.name = "kuna"
#
#     def happy(self):
#         print(self.name)
#
# class child(hi):
#     def __init__(self):
#         self.other_name = "common"
#         super().__init__()
#         self.name = "other kuna"
#
#     def one_more(self):
#         print(self.other_name)
#
# k = child()
# print(k.happy())


#method overriding
# class hi(object):
#     def __init__(self):
#         self.name = "kuna"
#
#     def happy(self):
#         print(self.name)
#
# class child(hi):
#     def __init__(self):
#         self.other_name = "common"
#         super().__init__()
#         self.name = "other kuna"
#
#     def one_more(self):
#         print(self.other_name)
#
#     def happy(self):
#         print("i am of child class only")
#
# k = child()
# print(k.happy())

#polymorphsim

# class hi(object):
#     def __init__(self):
#         self.name = "kuna"
#
#
#     def happy(self):
#         if "child" in str(self.__class__):
#             print(self.other_name)
#         elif "hi" in str(self.__class__):
#             print(self.name)
#
# class child(hi):
#     def __init__(self):
#         self.other_name = "common"
#         super().__init__()
#         self.name = "other kuna"
#
#     def one_more(self):
#         print(self.other_name)
#
# k = hi()
# k.happy()
#
# k = child()
# k.happy()


#dunders

# k = 1
# print(dir(k))
# print(k + 2)
#
# class happy():
#     def __init__(self, value, one_more):
#         self.value = value
#         self.one = one_more
#     def __add__(self, other_value):
#         return self.value + other_value + self.one
#
# k = happy(3, 3)
# print(k + 4)





