#!/usr/bin/env bash

#############
# Constants #
#############

# Versions
ARG=vagrant
INSTANT_CLIENT_VERSION=18.3
# Proxies
#HTTP_PROXY="http://www-proxy-hqdc.us.oracle.com:80/"
#HTTPS_PROXY="http://www-proxy-hqdc.us.oracle.com:80/"
#FTP_PROXY="http://www-proxy-hqdc.us.oracle.com:80/"
#NO_PROXY="us.oracle.com oraclecorp.com oraclevcn.com"
#EXPORT_PROXY="export http_proxy https_proxy ftp_proxy no_proxy"
# Files
#ORACLE_PROXY_FILE=/tmp/oracle_proxy.sh
SETUP_LOG=/opt/setup.log
# Arrays
declare -a REQUIRED_PACKAGES=("python36" "python36-devel" "python36-pip" "ansible" "git" "wget" "ntp" "ntpdate" \
        "zlib-devel" "libffi" "openssl-devel" "xz-libs" "xz-devel" "sqlite-devel" "readline-devel" "zlib-devel" \
        "python36-devel" "python36-setuptools" "httpd-devel" "make" "gcc" "gcc-c++" "unzip" "vim" "gcc" \
        "rpm-build" "rpm-devel" "rpmlint" "make" "coreutils" \
        "diffutils" "patch" "rabbitmq-server" "python-oci-sdk" "python-oci-cli")
declare -a REQUIRED_PYTHON_LIBS=("oci" "paramiko" "cryptography" "pytest==3.9.1" "pytest-cov==2.6.0" \
        "pytest-django==3.4.7" "pylint==2.1.1" "pyyaml==3.13" "Django" "Celery" "Django-Celery-Beat" "cx_oracle" \
        "python3-ldap" "requests" "flask" )


#function Add_Proxy_Details ()
#{
#    echo "#####################"
#    echo "# Adding proxy info #"
#    echo "#####################"
#    sleep 1
#    echo "http_proxy=\"${HTTP_PROXY}\"" > ${ORACLE_PROXY_FILE}
#    echo "https_proxy=\"${HTTPS_PROXY}\"" >> ${ORACLE_PROXY_FILE}
#    echo "ftp_proxy=\"${FTP_PROXY}\"" >> ${ORACLE_PROXY_FILE}
#    echo "no_proxy=\"${NO_PROXY}\"" >> ${ORACLE_PROXY_FILE}
#    echo "${EXPORT_PROXY}" >> ${ORACLE_PROXY_FILE}
#    chmod 755 ${ORACLE_PROXY_FILE}
#    log_status "oracle proxies"
#}

function update_OS ()
{
    echo "#####################"
    echo "# Updating OS       #"
    echo "#####################"
    yum -y update
    yum -y install unzip
    log_status "os update"
}

function Install_Required_Packages ()
{
    echo "################################"
    echo "# Installing required packages #"
    echo "################################"
    sleep 1
#    source ${ORACLE_PROXY_FILE}
    for package in "${REQUIRED_PACKAGES[@]}"; do
        yum -y install ${package} || Error_Exit "Failed to install ${package}. Aborting."
    done
    log_status "yum packages"
}

function Symlink_Python ()
{
    echo "####################"
    echo "#Python3 symlink   #"
    echo "####################"
    ln -s /usr/bin/python3.6 /usr/bin/python3 || Error_Exit "Failed to set python3 symlink. Aborting."

}

function Install_Required_Python_Libs ()
{
    echo "########################################"
    echo "# Installing required python libraries #"
    echo "########################################"
    sleep 1
    cd /root
#    source ${ORACLE_PROXY_FILE}
    # Apache needs to be running for mod_wsgi to install
    python3 -m pip install --upgrade pip
    for lib in "${REQUIRED_PYTHON_LIBS[@]}"; do
        python3 -m pip install ${lib} || Error_Exit "Failed to install ${lib}. Aborting."
    done

    log_status "python libraries"
}

function Symlink_Celery ()
{
    echo "####################"
    echo "# Celery symlink   #"
    echo "####################"
    ln -s /usr/local/lib/python3.6/site-packages/celery /usr/bin/celery

}

function Install_Terraform ()
{
    wget https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
    unzip terraform_0.11.8_linux_amd64.zip
    mv terraform /usr/local/bin
    log_status "terraform install"
}

function Install_Instant_Client ()
# Installs Oracle Instant Client.
{
    echo "#################################"
    echo "Installing Oracle Instant Client"
    echo "#################################"

    yum -y install /opt/rpms/oracle*
}

function LD_Library_Config ()
{
    echo "##############################"
    echo "# Updating LD_Library Config #"
    echo "##############################"
    sleep 1
    echo "/usr/lib/oracle/${INSTANT_CLIENT_VERSION}/client64/lib" >> /etc/ld.so.conf.d/oracle.conf
    #echo "${PYTHON_INSTALL_DIR}/lib" >> /etc/ld.so.conf.d/python3.conf
    ldconfig
}

function log_status ()
{
    if [ ! -f ${SETUP_LOG} ]
    then
        touch ${SETUP_LOG}
    fi

    echo "Task $1 is done." >> ${SETUP_LOG}

}

function Install_Private_SSH_Key ()
# Put your private key in oci/tf/.secret
{
    cp /vagrant/oci/tf/.secret/id_rsa /home/vagrant/.ssh/id_rsa
    chown vagrant: /home/vagrant/.ssh/id_rsa
    chmod 600 /home/vagrant/.ssh/id_rsa
}

function Configure_RabbitMQ ()
{
    systemctl start rabbitmq-server
    rabbitmqctl add_user pogonip pogonip_dev
    rabbitmqctl add_vhost pogonip_vhost
    rabbitmqctl set_user_tags pogonip pogonip_tag
    rabbitmqctl set_permissions -p pogonip_vhost pogonip ".*" ".*" ".*"

}

function main ()
{
    cd /opt/
#    Add_Proxy_Details
#    source /tmp/oracle_proxy.sh
    update_OS
    Install_Required_Packages
#    Install_Instant_Client
#    Symlink_Python
#    Symlink_Celery
    Install_Required_Python_Libs
#    Install_Terraform
#    Install_Private_SSH_Key
#    Configure_RabbitMQ

}

main